// console.log("Hello World");

// [SECTION] JavasScript Synchronous vs Asynchronous
// Asynchronous - means that we can proceed to execute other statements, while time consuming codes are running in the background.

// Fetch() method returns a promise that resolves to a response object.
// promise - is an object that represents the eventual completion (or failure) of an asynchronouse function and its resulting value.
console.log(fetch('https://jsonplaceholder.typicode.com/posts/'));

// Fetch() method returns a promise that resolves to a response object.
// then() captures the response object and returns another promise which will eventually be resolved or rejected.
fetch('https://jsonplaceholder.typicode.com/posts/').then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts/').then(response => response.json()).then((json) => console.log(json));


// async and await
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts/');
	console.log(result);
	console.log(typeof result);

	let json = await result.json();
	console.log(json);
};

fetchData();


// [SECTION] Creating a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the "request object."
	method: 'POST',
	// Sets the header data of the "request object" to be sent to the back end.
	headers: {
		'Content-Type': 'application/json'
	},
	// JSON.stringify - converts the object data into a stringified JSON.
	body: JSON.stringify({
  	  	"userId": 1,
		"title": "Create Post",
  	  	"body": "Create Post File"
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Mini-activity
// [SECTION] Updating a post using PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// Sets the method of the "request object."
	method: 'PUT',
	// Sets the header data of the "request object" to be sent to the back end.
	headers: {
		'Content-Type': 'application/json'
	},
	// JSON.stringify - converts the object data into a stringified JSON.
	body: JSON.stringify({
		"title": "Update Post",
  	  	"body": "Update Post File"
	})
}).then((response) => response.json()).then((json) => console.log(json));

// [SECTION] Deleting a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// Sets the method of the "request object."
	method: 'DELETE'});