fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
	let titles = data.map(item => item.title);
	console.log(titles);
})

fetch('https://jsonplaceholder.typicode.com/todos/100')
.then(response => response.json())
.then(data => {
	console.log(`Title: ${data.title} \nStatus: ${data.completed}`);
})

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
		"title": "Test to-do",
		"completed": true
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"title": "Update to-do",
		"completed": false
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/2', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"Title": "Update to-do",
		"Description": 'Updates the task',
		"Status": 'incomplete',
		"Date Completed": '',
		"User ID": 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/2', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"Status": 'completed',
		"Date Completed": '01-31-2023',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/3', {
	method: 'DELETE'})
.then((response) => response.json())
.then((json) => console.log(json));